gnx_cls = nx.clustering(gnx_m)


plt.rcParams.update(plt.rcParamsDefault)
plt.rcParams.update({'figure.figsize': (15, 10)})
plt.style.use('dark_background')

# Set node and edge communities
set_node_community(G_social, communities)
set_edge_community(G_social)

# Set community color for internal edges
external = [(v, w) for v, w in G_social.edges if G_social.edges[v, w]['community'] == 0]
internal = [(v, w) for v, w in G_social.edges if G_social.edges[v, w]['community'] > 0]
internal_color = ["black" for e in internal]
node_color = [get_color(G_social.nodes[v]['community']) for v in G_social.nodes]
# external edges
nx.draw_networkx(
        G_social, 
        pos=pos, 
        node_size=0, 
        edgelist=external, 
        edge_color="silver",
        node_color=node_color,
        alpha=0.2, 
        with_labels=False)
# internal edges
nx.draw_networkx(
        G_social, pos=pos, 

        edgelist=internal, 
        edge_color=internal_color,
        node_color=node_color,
        alpha=0.05, 
        with_labels=False)


node_sizes = [4000 if entry != 'Letter' else 1000 for entry in carac.type]

# Set node colors
cmap = matplotlib.colors.ListedColormap(['dodgerblue', 'lightgray', 'darkorange'])


# Subplot 1
plt.subplot(2, 2, 1)
nx.draw(G, with_labels=True, node_color=carac['type'].cat.codes, cmap=cmap, 
        node_size = node_sizes, edgecolors='gray')
plt.title('Spring Layout (Default)', fontsize=18)

# Subplot 2
plt.subplot(2, 2, 2)
nx.draw_random(G, with_labels=True, node_color=carac['type'].cat.codes, cmap=cmap, 
               node_size = node_sizes, edgecolors='gray')
plt.title('Random Layout', fontsize=18)

# Subplot 3
plt.subplot(2, 2, 3)
nx.draw_shell(G, with_labels=True, node_color=carac['type'].cat.codes, cmap=cmap, 
            node_size = node_sizes, edgecolors='gray')
plt.title('Shell Layout', fontsize=18)

nx_pylab.draw_kamada_kawai

#nodes contain a 1433 char long dictionary, with flags that you can check for a specific title or subject
feature_names = ["w_{}".format(i) for i in range(1433)]
column_names =  feature_names + ["subject"]
#Read in the node data with all the labels
node_data = pd.read_csv(os.path.join(data_dir, "cora.content"), sep='\t', header=None, names=column_names)

components = nx.connected_components(gnx)
largest_component = max(components, key=len)
gnx_l = gnx.subgraph(largest_component)