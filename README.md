# Network Project for Complex Adaptive Systems - MST698

**Introduction 
For the network analysis project I decided on the Cora Dataset (https://relational.fit.cvut.cz/dataset/CORA), primarily due to my interest in artificial intelligence as a subject and for curiousity in understanding the social relationships, and often "popularity contests", among academics. The Cora dataset is a massive relational database of publications in seven categories of artificial intelligence and machine learning, including: Case_Based', 'Genetic_Algorithms','Neural_Networks','Probabilistic_Methods' 'Reinforcement_Learning','Rule_Learning', and 'Theory.' The dataset cosnists of 2708 jounral publications (nodes), with 5429 links. The links or vertices are author to author citations and collaborations. The data attached to each node consists of a flag indicating whether a word in a 1433-long dictionary is present or not, for search, and is linked by the seven different subjects (ML categories above). Going through the data there does appear to be stong smallworld networks as you'd expect to see in tight academic communities, and close centrality in even smaller neighborhoods and cliques. I didn't have the time (or expertise likely) to dig into how these differences brak out among disciplines, individual papers, or indvidual scholars, and couldn't measure as mamy attributes as I had planned. Network analysis is defitniely addicting and I will likely spend a lot more time working with this network. 

**Dfficulties
Unfortunately, as a first time user of graph opjects and the networkx library, it really was a struggle to learn how to query the data, load the data, and for it to draw in a reasoable ammount of time; the size of the graph is the primary issue (and likely the memory on my personal laptop) and it really slowed me down with respect to identifying any significant analytical insights. Another negative, and I'll know this in the future, is Cora is a hugely popular dataset for graph machine learning and the vast majority of articels published on the data and reference material has to do with training anticipative neural networks. While this is very interesting material, I did have a hard time identifying easy step solutions to very basic problems. I rarely (or never) found an answer on stackexchange, w3school, etc, with respect to a simple quexstion on changing a node position or color, so spent most of the time scouring networkx library and trying to translate that information to the scale of Cora. 

**Basic Statistics and Discussion
As highlighted above the cora dataset has 5278 Edges and 2708 nodes. Out of the seven categories, Neural Networks makes up the papers written, citations, and cross citations, with 30 percent of the total (see histogram and scatterplot). Due to  the size of the dataset, I made subgraph using the max connected components method ((gnx.subgraph(c).copy() for c in nx.connected_components(gnx)). The subgraph (gnx_m) focused in on only the most highly conected cluster of relationships between the authors. This dropped my number of nodes and edges down slightly to 2485 nodes and 5069 edges, so not as maximaly as I would've liked, but it mained the importent connected nodes and it did cut the single connections from the group (although those could've been interesting in themselves). I tried a second subgraph using the k-edge method ((gnx.subgraph(c).copy() for c in nx.k_edge_subgraphs(gnx, k=3))). The "k-edge-augmentation is a set of edges, that once added to a graph, ensures that the graph is k-edge-connected" whince in principle ensure maximal connectivieyt between the subgroup with minimum weight (https://networkx.org/documentation/stable/reference/algorithms/

I did some quick statistics on the cora dataset and subset gnx_m:
**Cora Dataset:
- Cora Network density: 0.0014399999126942077
- Cora Triadic closure: 0.09349725626661058
- Max Conected Components: 79
- K-edge connected components: {3: [{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}], 2: [{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}], 1: [{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}]}
**Max_Connected Components Subset:
- Max conected compoents of subgraph: 
- Network density: 0.0016423824752055003
- Triadic closure: 0.0900352512858051
**Communities


import networkx.algorithms.community as nxcom

communities = sorted(nxcom.greedy_modularity_communities(gnx_m), key=len, reverse=True)
len(communities)

**sorted(nxcom.greedy_modularity_communities(gnx_m)
**29 Communities found


ba_sub = nx.barabasi_albert_graph(2708, 1267)
degrees = list(nx.degree(gnx))
l = [d[1] for d in degrees]



**k_components = nx.k_components(G)
print(k_components)
{3: [{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}], 2: [{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}], 1: [{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}]}

nx.sigma(gnx_m, niter=10, nrand=5)
(??)

**nxcom.girvan_newman(gnx_m)
communities = next(result)
len(communities)

**2 Communities found

cliques = list(nx.find_cliques(gnx_m))
max_clique = max(cliques, key=len)
node_color = [(0.5, 0.5, 0.5) for v in gnx_m.nodes()]
for i, v in enumerate(gnx_m.nodes()):
    if v in max_clique:
        node_color[i] = (0.5, 0.5, 0.9)



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:5b126d984e065697f4957029962feb93?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:5b126d984e065697f4957029962feb93?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:5b126d984e065697f4957029962feb93?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/kyle.a.kilian/network-project-repo_two.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:5b126d984e065697f4957029962feb93?https://gitlab.com/kyle.a.kilian/network-project-repo_two/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:5b126d984e065697f4957029962feb93?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:5b126d984e065697f4957029962feb93?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:5b126d984e065697f4957029962feb93?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://gitlab.com/-/experiment/new_project_readme_content:5b126d984e065697f4957029962feb93?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:5b126d984e065697f4957029962feb93?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:5b126d984e065697f4957029962feb93?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:5b126d984e065697f4957029962feb93?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:5b126d984e065697f4957029962feb93?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:5b126d984e065697f4957029962feb93?https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://gitlab.com/-/experiment/new_project_readme_content:5b126d984e065697f4957029962feb93?https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

