Metrics:
Embeddings - (2708, 32)
The GraphSAGE embeddings are the output of the GraphSAGE layers, namely the x_out 
variable. Let's create a new model with the same inputs as we used previously x_inp but now the output is the embeddings rather than the predicted class. 
Additionally note that the weights trained previously are kept in the new model.

